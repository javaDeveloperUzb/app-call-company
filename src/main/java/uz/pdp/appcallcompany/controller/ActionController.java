package uz.pdp.appcallcompany.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.appcallcompany.payload.ReqAction;
import uz.pdp.appcallcompany.service.ActionService;

@RestController
@RequestMapping("/action")
public class ActionController {
    @Autowired
    ActionService actionService;
    @PostMapping
    public HttpEntity<?> addAction(@RequestBody ReqAction reqAction) {
        actionService.saveAction(reqAction);
        return null;
    }
}
