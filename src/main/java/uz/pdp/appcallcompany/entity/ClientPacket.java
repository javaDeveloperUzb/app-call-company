package uz.pdp.appcallcompany.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.appcallcompany.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@PackagePrivate
@Entity
@EqualsAndHashCode(callSuper = true)
public class ClientPacket extends AbsEntity {
    @ManyToOne
    ClientPhoneNumber clientPhoneNumber;
    @ManyToOne
    Packet aPacket;
    long leftover;
    Timestamp expireDate;

}
