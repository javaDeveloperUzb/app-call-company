package uz.pdp.appcallcompany.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.PackagePrivate;
import uz.pdp.appcallcompany.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@PackagePrivate
public class ClientTariffExpenseType extends AbsEntity {
    @ManyToOne
    ExpenseType expenseType;
    long leftover;
    @ManyToOne
    ClientPhoneNumber clientPhoneNumber;
    @ManyToOne
    Tariff tariff;
    Timestamp deadLine;
}
