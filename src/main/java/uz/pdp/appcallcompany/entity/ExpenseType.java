package uz.pdp.appcallcompany.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.appcallcompany.entity.template.AbsNameEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@PackagePrivate
public class ExpenseType extends AbsNameEntity {
    @ManyToOne
    ActionType actionType;
    @ManyToOne
    ExpenseType expenseType;
}
