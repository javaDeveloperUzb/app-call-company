package uz.pdp.appcallcompany.entity.enums;

public enum  PersonType {
    JURIDICAL, //yudik shaxs
    PHYSICAL //jismoniy shaxs
}
