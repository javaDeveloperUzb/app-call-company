package uz.pdp.appcallcompany.entity.template;

import lombok.Data;
import lombok.experimental.PackagePrivate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@PackagePrivate
@Data
@MappedSuperclass
public abstract class AbsNameEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    @Column(unique = true)
    String nameUz;
    @Column(unique = true)
    String nameRu;
    @Column(unique = true)
    String nameEn;
}
