package uz.pdp.appcallcompany.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.PackagePrivate;
import uz.pdp.appcallcompany.entity.template.AbsNameEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@PackagePrivate
public class Packet extends AbsNameEntity {
    double price;
    Integer duration;
    @OneToMany
    List<ExpenseType> expenseTypes;
    long amount;
    boolean archive;

}
