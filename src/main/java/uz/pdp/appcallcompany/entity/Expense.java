package uz.pdp.appcallcompany.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.PackagePrivate;
import uz.pdp.appcallcompany.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@PackagePrivate
@Entity
public class Expense extends AbsEntity {
    @ManyToOne
    ExpenseType expenseType;
    double sum;
    long amount;

    @ManyToOne
    ClientPhoneNumber clientPhoneNumber;
}
