package uz.pdp.appcallcompany.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.appcallcompany.entity.enums.PersonType;
import uz.pdp.appcallcompany.entity.template.AbsEntity;
import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@PackagePrivate
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"passportSerial","passportNumber"})})
public class  Client extends AbsEntity {
    @Column(nullable = false)
    String firstName;
    @Column(nullable = false)
    String lastName;
    @Column(nullable = false)
    String middleName;
    @Column(nullable = false)
    Date birthDay;
    @Column(nullable = false)

    String passportSerial;
    @Column(nullable = false)
    String passportNumber;
    @Column(nullable = false)

    String givenPlace; //Berilgan joyi IIB ...
    @Column(nullable = false)
    Date givenDate; //Berilgan vaqti  ...
    @Column(nullable = false)
    Date expireDate; //amal qilish vaqti
    @Column(nullable = false)
    String address; //manzili
    @Enumerated(EnumType.STRING)
    PersonType personType; //enum


}
