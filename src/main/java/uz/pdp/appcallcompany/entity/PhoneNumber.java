package uz.pdp.appcallcompany.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.appcallcompany.entity.template.AbsEntity;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EqualsAndHashCode(callSuper = true)
@PackagePrivate
public class PhoneNumber extends AbsEntity  {
    String number;
    @ManyToOne
    Category category;

    Boolean sold;
}
