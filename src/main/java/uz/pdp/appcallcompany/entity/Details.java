package uz.pdp.appcallcompany.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.PackagePrivate;
import uz.pdp.appcallcompany.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Data
@Entity
@PackagePrivate
@EqualsAndHashCode(callSuper = true)
public class Details extends AbsEntity {
    @ManyToOne
    ClientPhoneNumber clientPhoneNumber;

    @ManyToOne
    ActionType actionType;

    long amount;
    String secondSide;
    boolean from;

}
