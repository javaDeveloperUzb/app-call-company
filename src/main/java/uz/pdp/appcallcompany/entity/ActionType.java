package uz.pdp.appcallcompany.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.PackagePrivate;
import uz.pdp.appcallcompany.entity.template.AbsNameEntity;

import javax.persistence.Entity;

@Data
@Entity
@PackagePrivate
@EqualsAndHashCode(callSuper = true)
public class ActionType extends AbsNameEntity {

}
