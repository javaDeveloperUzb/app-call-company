package uz.pdp.appcallcompany.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.appcallcompany.entity.enums.PersonType;
import uz.pdp.appcallcompany.entity.template.AbsNameEntity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@PackagePrivate
public class Tariff extends AbsNameEntity {
    boolean archive;
    double price;
    long mb;
    Integer sms;
    Integer minuteIn;
    Integer minuteOut;
    @Enumerated(EnumType.STRING)
    PersonType personType;
    @OneToMany(mappedBy = "tariff")
    List<TariffExpenseTypeAfterLimit> tariffExpenseTypeAfterLimits;
}
