package uz.pdp.appcallcompany.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.PackagePrivate;
import uz.pdp.appcallcompany.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@PackagePrivate
public class TariffExpenseTypeAfterLimit extends AbsEntity {
    @ManyToOne
    Tariff tariff;
    @ManyToOne
    ExpenseType expenseType;
    double price;
}
