package uz.pdp.appcallcompany.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.appcallcompany.entity.template.AbsEntity;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@PackagePrivate
@Entity
public class ClientPhoneNumber extends AbsEntity {
    @ManyToOne
    Client client;
    @ManyToOne
    PhoneNumber phoneNumber;
    boolean enabled;
    double balance;

    @ManyToOne
    Tariff tariff;
}
