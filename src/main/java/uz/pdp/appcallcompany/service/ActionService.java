package uz.pdp.appcallcompany.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import uz.pdp.appcallcompany.entity.*;
import uz.pdp.appcallcompany.payload.ApiResponse;
import uz.pdp.appcallcompany.payload.ReqAction;
import uz.pdp.appcallcompany.repository.*;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ActionService {
    @Autowired
    ClientPhoneNumberRepository clientPhoneNumberRepository;
    @Autowired
    ClientPacketRepository clientPacketRepository;
    @Autowired
    PacketRepository packetRepository;
    @Autowired
    ExpenseTypeRepository expenseTypeRepository;
    @Autowired
    DetailsRepository detailsRepository;
    @Autowired
    ClientTariffExpenseTypeRepository clientTariffExpenseTypeRepository;

    public ApiResponse saveAction(ReqAction reqAction) {
        Optional<ClientPhoneNumber> optionalClient = clientPhoneNumberRepository.findById(reqAction.getClientId());
        if (optionalClient.isPresent()) {
            ClientPhoneNumber clientPhoneNumber = optionalClient.get(); //optionalClient.get() qildik
            if (clientPhoneNumber.isEnabled()) {
                ExpenseType expenseType = expenseTypeRepository.findById(reqAction.getExpenseTypeId()).orElseThrow(() -> new ResourceNotFoundException("getExpenseType"));//is present qimasdan xatoni korsatish  lambda orqali otivorish
                if (!reqAction.isFrom()) {
                    saveDetails(reqAction, expenseType, clientPhoneNumber);
                    return new ApiResponse("Amal muvaffaqiyatli yakunlandi", true);
                } else {
                    List<Packet> packetList = packetRepository.findAllByExpenseTypesInAndArchiveFalse(Collections.singletonList(expenseType));
                    List<ClientPacket> clientPackets = clientPacketRepository.findAllByClientPhoneNumberAndExpireDateBeforeAndLeftoverGreaterThanAndAPacketInOrderOrderByCreatedAt(clientPhoneNumber, new Timestamp(new Date().getTime()), 0, packetList);
                    long yechilgani = 0;
                    for (ClientPacket clientPacket : clientPackets) {
                        if (clientPacket.getLeftover() > reqAction.getAmount() - yechilgani) {
                            yechilgani += (reqAction.getAmount() - yechilgani);
                            clientPacket.setLeftover(clientPacket.getLeftover() - (reqAction.getAmount() - yechilgani));
                            break;
                        } else {
                            yechilgani += clientPacket.getLeftover();
                            clientPacket.setLeftover(0);
                        }
                    }
                    if (yechilgani == reqAction.getAmount()) {
                        clientPacketRepository.saveAll(clientPackets);
                        saveDetails(reqAction, expenseType, clientPhoneNumber);
                        return new ApiResponse("Amal muvaffaqiyatli yakunlandi", true);
                    } else {

                        if (yechilgani == reqAction.getAmount()) {
                            saveDetails(reqAction,  expenseType,clientPhoneNumber);
                        }
                    }
                }
            }
            return new ApiResponse("Raqamingiz bloklangan", false);
        }
        return new

                ApiResponse("Bunday user mavjuda emas", false);

    }

    void saveDetails(ReqAction reqAction, ExpenseType expenseType, ClientPhoneNumber clientPhoneNumber) {
        Details details = new Details();
        details.setAmount(reqAction.getAmount());
        details.setActionType(expenseType.getActionType());
        details.setClientPhoneNumber(clientPhoneNumber);
        details.setFrom(reqAction.isFrom());
        details.setSecondSide(reqAction.getSecondSide());
        detailsRepository.save(details);
    }

    ExpenseType getUntilParentExpenseType(long amount,long yechilgani,ExpenseType expenseType,ClientPhoneNumber clientPhoneNumber) {
        if (amount == yechilgani) {
            return null;
        } else {
            Optional<ClientTariffExpenseType> optionalClientTariffExpenseType = clientTariffExpenseTypeRepository.findByExpenseTypeAndClientPhoneNumberAndTariffAndDeadLineAfter(expenseType, clientPhoneNumber, clientPhoneNumber.getTariff(), new Timestamp(new Date().getTime()));
            if (optionalClientTariffExpenseType.isPresent()) {
                ClientTariffExpenseType clientTariffExpenseType = optionalClientTariffExpenseType.get();
                if (clientTariffExpenseType.getLeftover() > (amount - yechilgani)) {
                    clientTariffExpenseType.setLeftover(clientTariffExpenseType.getLeftover() - (amount- yechilgani));
                    yechilgani = amount;
                } else {
                    yechilgani += clientTariffExpenseType.getLeftover();
                    clientTariffExpenseType.setLeftover(0);
                }
                clientTariffExpenseTypeRepository.save(clientTariffExpenseType);
            } else {
                if (expenseType.getExpenseType()!=null) {
                    getUntilParentExpenseType(amount, yechilgani, expenseType.getExpenseType(), clientPhoneNumber);
                } else {
                    return null;
                }
            }
        }
        return null;
    }
}
