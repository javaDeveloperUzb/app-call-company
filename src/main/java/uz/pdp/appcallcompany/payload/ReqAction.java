package uz.pdp.appcallcompany.payload;

import lombok.Data;
import lombok.experimental.PackagePrivate;

import java.util.Date;
import java.util.UUID;

@Data
@PackagePrivate
public class ReqAction {
    UUID clientId;
    Integer expenseTypeId;
    long amount;
    String secondSide;
    boolean from;

}
