package uz.pdp.appcallcompany.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@PackagePrivate
public class ApiResponse {
    String message;
    boolean success;

}
