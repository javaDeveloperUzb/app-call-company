package uz.pdp.appcallcompany.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appcallcompany.entity.ClientPacket;
import uz.pdp.appcallcompany.entity.ClientPhoneNumber;
import uz.pdp.appcallcompany.entity.ExpenseType;
import uz.pdp.appcallcompany.entity.Packet;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

public interface PacketRepository extends JpaRepository<Packet, Integer> {
    List<Packet> findAllByExpenseTypesInAndArchiveFalse(List<ExpenseType> expenseTypes);
}
