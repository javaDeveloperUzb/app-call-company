package uz.pdp.appcallcompany.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appcallcompany.entity.*;

import java.sql.Timestamp;
import java.util.Optional;
import java.util.UUID;

public interface ClientTariffExpenseTypeRepository extends JpaRepository<ClientTariffExpenseType, UUID> {
    Optional<ClientTariffExpenseType> findByExpenseTypeAndClientPhoneNumberAndTariffAndDeadLineAfter(ExpenseType expenseType, ClientPhoneNumber clientPhoneNumber, Tariff tariff, Timestamp deadLine);
}
