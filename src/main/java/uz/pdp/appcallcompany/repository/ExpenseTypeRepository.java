package uz.pdp.appcallcompany.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appcallcompany.entity.ExpenseType;
import uz.pdp.appcallcompany.entity.Packet;

import java.util.List;

public interface ExpenseTypeRepository extends JpaRepository<ExpenseType, Integer> {
}
