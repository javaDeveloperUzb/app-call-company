package uz.pdp.appcallcompany.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appcallcompany.entity.Details;

import java.util.UUID;

public interface DetailsRepository extends JpaRepository<Details, UUID> {

}
