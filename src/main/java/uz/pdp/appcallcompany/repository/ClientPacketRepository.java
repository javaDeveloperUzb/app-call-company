package uz.pdp.appcallcompany.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appcallcompany.entity.ClientPacket;
import uz.pdp.appcallcompany.entity.ClientPhoneNumber;
import uz.pdp.appcallcompany.entity.Packet;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

public interface ClientPacketRepository extends JpaRepository<ClientPacket, UUID> {
    List<ClientPacket> findAllByClientPhoneNumberAndExpireDateBeforeAndLeftoverGreaterThanAndAPacketInOrderOrderByCreatedAt(ClientPhoneNumber clientPhoneNumber, Timestamp expireDate, long leftover, Collection<Packet> APacket);
}
